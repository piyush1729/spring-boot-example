package com.db.connection.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.db.connection.entity.ActorEntity;

public interface ActorDao extends JpaRepository<ActorEntity, Long> {
	
	public List<ActorEntity> findByFname(String actorName);

}
