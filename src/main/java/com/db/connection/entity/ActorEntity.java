package com.db.connection.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "actor")
public class ActorEntity {
	
	@Id
	@Column(name = "actor_id")
	private int actorId;
	
	
	@Column(name = "first_name")
	private String fname;
	
	
	@Column(name = "last_name")
	private String lname;
	
	
	/*
	 * @Column(name = "last_update") private LocalDateTime lastUpdatedDate;
	 */


	public int getActorId() {
		return actorId;
	}


	public void setActorId(int actorId) {
		this.actorId = actorId;
	}


	public String getFname() {
		return fname;
	}


	public void setFname(String fname) {
		this.fname = fname;
	}


	public String getLname() {
		return lname;
	}


	public void setLname(String lname) {
		this.lname = lname;
	}


	/*
	 * public LocalDateTime getLastUpdatedDate() { return lastUpdatedDate; }
	 * 
	 * 
	 * public void setLastUpdatedDate(LocalDateTime lastUpdatedDate) {
	 * this.lastUpdatedDate = lastUpdatedDate; }
	 */


	

}
