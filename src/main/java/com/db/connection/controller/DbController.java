package com.db.connection.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.db.connection.model.Actor;
import com.db.connection.service.DbService;


@RestController
@RequestMapping("/api")
public class DbController {
	
	@Autowired
	DbService dbService;
	
	
	@GetMapping("/data")
	public List<Actor> getData() {
		
		//DbService dbService=new DbService();
		
		return dbService.testDbConnection();
	} 
	
	
	@PostMapping("/data")
	public List<Actor> getDatas() {
		
		//DbService dbService=new DbService();
		
		return dbService.testDbConnection();
		
		
	} 

}
