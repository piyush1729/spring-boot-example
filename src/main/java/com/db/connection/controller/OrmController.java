package com.db.connection.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.db.connection.entity.ActorEntity;
import com.db.connection.service.ActorService;

@RestController
@RequestMapping("/api")
public class OrmController {
	
	
	@Autowired
	ActorService actorService;
	
	@GetMapping("/testingOrm")
	public List<ActorEntity> connection() {
		
		
		return actorService.getAllActor();
	}
	
	
	@GetMapping("/actor")
	public List<ActorEntity> actor(@RequestParam(name = "actorName")  String actorName) {
		
		
		return actorService.getActorByName(actorName);
	}

}
