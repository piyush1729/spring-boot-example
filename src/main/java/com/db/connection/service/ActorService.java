package com.db.connection.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db.connection.dao.ActorDao;
import com.db.connection.entity.ActorEntity;

@Service
public class ActorService {

	@Autowired
	ActorDao actorDao;

	public List<ActorEntity> getAllActor() {
		List<ActorEntity> listOfActor = actorDao.findAll();
		
		return listOfActor;
	}
	
	
	public List<ActorEntity> getActorByName(String actorName) {
		
		
		List<ActorEntity> listOfActor = actorDao.findByFname(actorName);
		
		return listOfActor;
	}

}
