package com.db.connection.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db.connection.dao.ConnectionDao;
import com.db.connection.model.Actor;

@Service
public class DbService {

	@Autowired
	ConnectionDao connectionDao;

	static {

		System.out.println("testing");
	}

	public List<Actor> testDbConnection() {

		// ConnectionDao connectionDao=new ConnectionDao();

		try {

			return connectionDao.testConnection();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
