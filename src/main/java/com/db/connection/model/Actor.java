package com.db.connection.model;

public class Actor {
	
	private String fname;
	
	private String lname;
	

	public Actor(String fname,String lname) {
		
		this.fname=fname;
		this.lname=lname;
	}
	
	public Actor() {
		
	}
	

	public String getFname() {
		
		return fname;
	}

	public void setFname(String fname) {
		
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}
	
}
